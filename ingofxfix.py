import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser(description="Fix up ING Australia's fucked up OFX files")
    parser.add_argument("-i", "--input-file", dest="input_file", required=True, help="The file containing dodgy OFX")
    parser.add_argument("-o", "--output-file", dest="output_file", required=True, help="The file which should contain less dodgy OFX")
    parser.add_argument("--bsb", dest="bsb", required=False, default="923100", help="The bank identifier to use in output")
    parser.add_argument("--account-number", dest="account_number", required=True, help="The account number to use in output")
    parser.add_argument("--account-type", dest="account_type", required=False, default="CURRENT", help="The account type to use in output")
    parser.add_argument("--start-date", dest="start_date", required=False, help="Throw away any transactions before this date")
    return parser.parse_args()

def read_ofx_file(input_file):
    with open(input_file,'r') as file:
        yield from file.readlines()

class OfxLine(object):
    def __init__(self,line):
        self.line = line.strip()

    def is_bank_account_from_line(self):
        return self.line == "<BANKACCTFROM>"

    def is_bank_transaction_list_line(self):
        return self.line == "<BANKTRANLIST>"

    def is_transaction_start(self):
        return self.line == "<STMTTRN>"

    def is_transaction_end(self):
        return self.line == "</STMTTRN>"

    def is_linetype(self, linetype=None):
        return self.line.startswith("<{}>".format(linetype))

    def get_value(self):
        return self.line.split(">")[1]

class OfxTransaction(object):

    def __init__(self):
        self.input_lines = []

    def add_line(self, ofxline):
        self.input_lines.append(ofxline)

    def output_lines(self):
        for ofxline in self.input_lines:
            if ofxline.is_linetype("FITID"):
                yield "{}.{}.{}".format(ofxline.line, self.get_value("DTPOSTED"), self.get_value("TRNAMT"))
            else:
                yield ofxline.line

    def get_value(self, linetype=None):
        for ofxline in self.input_lines:
            if ofxline.is_linetype(linetype):
                return ofxline.get_value()


def fix_ofx(input_ofx_lines, bank_account_details, transaction_filter=lambda t: True):
    """
    1. add missing BANKACCTFROM entry if it's not there
    2. ensure FITIDs are unique by appending DTPOSTED and TRNAMT
    3. filter any transactions which do not match the filter provided
    """
    bank_account_details_seen = False
    current_transaction = None
    ofxlines = (OfxLine(line) for line in input_ofx_lines)
    for ofxline in ofxlines:
        bank_account_details_seen = bank_account_details_seen or ofxline.is_bank_account_from_line()
        if ofxline.is_bank_transaction_list_line() and not bank_account_details_seen:
            yield from bank_account_details
        if current_transaction:
            if ofxline.is_transaction_end():
                current_transaction.add_line(ofxline)
                if transaction_filter(current_transaction):
                    yield from current_transaction.output_lines()
                current_transaction = None
            else:
                current_transaction.add_line(ofxline)
        else:
            if ofxline.is_transaction_start():
                current_transaction = OfxTransaction()
                current_transaction.add_line(ofxline)
            else:
                yield ofxline.line




def write_ofx_file(output_file, output_ofx):
    with open(output_file, 'w') as file:
        for line in output_ofx:
            file.write(line)
            file.write(os.linesep)


def generate_bank_account_details(bsb, account_number, account_type):
    return [
        "<BANKACCTFROM>",
        "<BANKID>{}".format(bsb),
        "<ACCTID>{}".format(account_number),
        "<ACCTTYPE>{}".format(account_type),
        "</BANKACCTFROM>"
    ]


def transaction_filter(start_date):
    if start_date is None:
        return lambda td : True
    else:
        return lambda td : td.get_value("DTPOSTED")[0:len(start_date)] >= start_date


if __name__ == "__main__":
    args = parse_args()
    input_ofx = read_ofx_file(args.input_file)
    bank_account = generate_bank_account_details(args.bsb,args.account_number,args.account_type)
    output_ofx = fix_ofx(input_ofx, bank_account, transaction_filter(args.start_date))
    write_ofx_file(args.output_file, output_ofx)
