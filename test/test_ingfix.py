import os

import pytest

from ingofxfix import fix_ofx, generate_bank_account_details

input = """OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>20160708104407
<LANGUAGE>ENG
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>1
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>AUD
<BANKTRANLIST>
<STMTTRN>
<TRNTYPE>CREDIT
<DTPOSTED>20160630000000
<TRNAMT>5.23
<FITID>903889
<MEMO>Bonus Interest Credit - Receipt 903889
</STMTTRN>
<STMTTRN>
<TRNTYPE>CREDIT
<DTPOSTED>20160630000000
<TRNAMT>10.47
<FITID>903889
<MEMO>Interest Credit
</STMTTRN>
</BANKTRANLIST>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>
"""

expected_output = """OFXHEADER:100
DATA:OFXSGML
VERSION:102
SECURITY:NONE
ENCODING:USASCII
CHARSET:1252
COMPRESSION:NONE
OLDFILEUID:NONE
NEWFILEUID:NONE
<OFX>
<SIGNONMSGSRSV1>
<SONRS>
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<DTSERVER>20160708104407
<LANGUAGE>ENG
</SONRS>
</SIGNONMSGSRSV1>
<BANKMSGSRSV1>
<STMTTRNRS>
<TRNUID>1
<STATUS>
<CODE>0
<SEVERITY>INFO
</STATUS>
<STMTRS>
<CURDEF>AUD
<BANKACCTFROM>
<BANKID>923100
<ACCTID>12345678
<ACCTTYPE>SAVINGS
</BANKACCTFROM>
<BANKTRANLIST>
<STMTTRN>
<TRNTYPE>CREDIT
<DTPOSTED>20160630000000
<TRNAMT>5.23
<FITID>903889.20160630000000.5.23
<MEMO>Bonus Interest Credit - Receipt 903889
</STMTTRN>
<STMTTRN>
<TRNTYPE>CREDIT
<DTPOSTED>20160630000000
<TRNAMT>10.47
<FITID>903889.20160630000000.10.47
<MEMO>Interest Credit
</STMTTRN>
</BANKTRANLIST>
</STMTRS>
</STMTTRNRS>
</BANKMSGSRSV1>
</OFX>"""


@pytest.mark.unit
def test_fix():
    bank_account_details = generate_bank_account_details("923100","12345678","SAVINGS")
    actual_output = fix_ofx(input.splitlines(),bank_account_details)
    assert expected_output == os.linesep.join(actual_output)


